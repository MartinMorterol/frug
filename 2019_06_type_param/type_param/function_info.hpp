#pragma once

#include <tuple>

template <class ReturnType, class... Args>
struct function_info
{
    constexpr static size_t arity = sizeof...(Args);

    template <size_t indice>
    struct arg_type_
    {
        static_assert((indice < arity), "La fonction à moins de param que ce que vous demandez");
        using type = typename std::tuple_element<indice, std::tuple<Args...>>::type;
    };

    using return_type = ReturnType;

    template <size_t i>
    using arg_type = typename arg_type_<i>::type;
};