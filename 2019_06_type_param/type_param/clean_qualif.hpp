#pragma once

// Note : befor GCC7 and clang4 noexcept was not well supported in specialisation
template <class T>
struct clean_calif
{
    using type = T;
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args...)>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args...) noexcept>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args...) &>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args...) & noexcept>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args...) &&>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args...) && noexcept>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args...) volatile>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args...) volatile noexcept>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args...) volatile &>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args...) volatile &noexcept>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args...) volatile &&>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args...) volatile &&noexcept>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args...) const>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args...) const noexcept>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args...) const &>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args...) const &noexcept>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args...) const &&>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args...) const &&noexcept>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args...) const volatile>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args...) const volatile noexcept>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args...) const volatile &>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args...) const volatile &noexcept>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args...) const volatile &&>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args...) const volatile &&noexcept>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args..., ...)>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args..., ...) noexcept>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args..., ...) &>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args..., ...) & noexcept>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args..., ...) &&>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args..., ...) && noexcept>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args..., ...) volatile>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args..., ...) volatile noexcept>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args..., ...) volatile &>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args..., ...) volatile &noexcept>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args..., ...) volatile &&>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args..., ...) volatile &&noexcept>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args..., ...) const>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args..., ...) const noexcept>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args..., ...) const &>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args..., ...) const &noexcept>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args..., ...) const &&>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args..., ...) const &&noexcept>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args..., ...) const volatile>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args..., ...) const volatile noexcept>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args..., ...) const volatile &>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args..., ...) const volatile &noexcept>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args..., ...) const volatile &&>
{
    using type = Return (Class::*)(Args...);
};

template <class Class, class Return, class... Args>
struct clean_calif<Return (Class::*)(Args..., ...) const volatile &&noexcept>
{
    using type = Return (Class::*)(Args...);
};

template <class T>
using clean_calif_t = typename clean_calif<T>::type;
