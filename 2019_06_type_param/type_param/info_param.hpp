#include "clean_qualif.hpp"
#include "function_info.hpp"

template <class... T>
class ERREUR;

template <class T>
struct informationParam_impl
{
    struct IsNotACallable
    {
    };
    ERREUR<T, IsNotACallable> erreur; // type in the error message
};

// pointeur de methode
template <typename ClassType, typename ReturnType, typename... Args>
struct informationParam_impl<ReturnType (ClassType::*)(Args...)>
{
    using type = function_info<ReturnType, Args...>;
};

// pointeur de fonction
template <typename ReturnType, typename... Args>
struct informationParam_impl<ReturnType (*)(Args...)>
{
    using type = function_info<ReturnType, Args...>;
};

// pointeur de fonction variadic
template <typename ReturnType, typename... Args>
struct informationParam_impl<ReturnType (*)(Args..., ...)>
{
    using type = function_info<ReturnType, Args...>;
};

template <
    class T,
    class U = void>
struct type_to_ptr_operator_parenthesis
{
    using type = T;
};

template <class T>
struct type_to_ptr_operator_parenthesis<
    T,
    std::void_t<
        decltype(&T::operator())>>
{
    using type = decltype(&T::operator());
};
template <class T>
using type_to_ptr_operator_parenthesis_t = typename type_to_ptr_operator_parenthesis<T>::type;

template <class T, size_t Nb>
using type_param = typename informationParam_impl<
    clean_calif_t<
        type_to_ptr_operator_parenthesis_t<
            std::decay_t<T>>>>::type::template arg_type<Nb>;
