#pragma once

template <class U, typename T = void>
struct can_be_checked : public std::false_type
{
};

template <typename U>
struct can_be_checked<U, std::enable_if_t<std::is_function_v<std::remove_pointer_t<U>>>> : public std::true_type
{
};

template <typename U>
struct can_be_checked<U, std::void_t<decltype(&U::operator())>> : public std::true_type
{
};
