#include <chrono>
#include <iostream>
#include <type_traits>
#include <vector>
using namespace std;
struct S_exp
{
    S_exp() { cout << "S_exp construtor" << endl; }
    S_exp(const S_exp &) { cout << "S_exp construtor copy" << endl; }
    S_exp(S_exp &&) { cout << "S_exp construtor move" << endl; }
};

struct S_del
{
    S_del() { cout << "S_del construtor" << endl; }
    S_del(const S_del &) = delete;
    S_del(S_del &&) noexcept(false) { cout << "S_del construtor move" << endl; }
};

struct S
{
    S() { cout << "S construtor" << endl; }
    S(const S &) { cout << "S construtor copy" << endl; }
    S(S &&)
    noexcept { cout << "S construtor move" << endl; }
};

int main()
{
    {
        std::cout << "can S_exp be nothrow move construct? => " << boolalpha << std::is_nothrow_move_constructible<S_exp>::value << std::endl;
        std::vector<S_exp> v1;
        v1.emplace_back();
        v1.emplace_back();
    }
    cout << endl
         << endl;
    {
        std::cout << "can S be nothrow move construct ? => " << boolalpha << std::is_nothrow_move_constructible<S>::value << std::endl;
        std::vector<S> v1;
        v1.emplace_back();
        v1.emplace_back();
    }
    cout << endl
         << "If copy is delete and move can throw ? "
         << endl;
    {
        std::cout << "can S_del be nothrow move construct ? => " << boolalpha << std::is_nothrow_move_constructible<S_del>::value << std::endl;
        std::vector<S_del> v1;
        v1.emplace_back();
        v1.emplace_back();
    }

    return 0;
}