#include <iostream>
#include <vector>

using namespace std;

namespace detail
{

template <class T>
struct foo_impl
{
    static void foo(T) { cout << __PRETTY_FUNCTION__ << endl; }
};

template <class T>
struct foo_impl<std::vector<T>>
{
    static void foo(std::vector<T>) { cout << "spe " << __PRETTY_FUNCTION__ << endl; }
};

} // namespace detail

template <class T>
void foo(T t)
{
    detail::foo_impl<T>::foo(t);
}
/*
template <class T>
void foo<std::vector<T>>(std::vector<T>) {} // KO
*/
void foo(double)
{
    cout << __PRETTY_FUNCTION__ << endl;
}

template <class T>
struct TD;
int main()
{
    foo(string{});
    foo(double{});
    foo(std::vector<int>{});
    foo(std::vector<string>{});
    return 0;
}