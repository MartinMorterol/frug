#include <iostream>
#include <type_traits>

#include "./type_param/function_info.hpp"
#include "./type_param/clean_qualif.hpp"
#include "./type_param/info_param.hpp"

struct S
{
    void foo0() {}
    void foo1() noexcept {}
    void foo2() & {}
    void foo3() & noexcept {}
    void foo4() && {}
    void foo5() && noexcept {}
    void foo6() volatile {}
    void foo7() volatile noexcept {}
    void foo8() volatile & {}
    void foo9() volatile &noexcept {}
    void foo10() volatile && {}
    void foo11() volatile &&noexcept {}
    void foo12() const {}
    void foo13() const noexcept {}
    void foo14() const & {}
    void foo15() const &noexcept {}
    void foo16() const && {}
    void foo17() const &&noexcept {}
    void foo18() const volatile {}
    void foo19() const volatile noexcept {}
    void foo20() const volatile & {}
    void foo21() const volatile &noexcept {}
    void foo22() const volatile && {}
    void foo23() const volatile &&noexcept {}

    void test(int, double) const volatile && {}
    void test_ptr_cc(int const *const) {}
};

int main()
{
    {
        using type = function_info<int>;
        static_assert(std::is_same_v<typename type::return_type, int>);
        static_assert(type::arity == 0);
    }

    {
        using type = function_info<double, char *, const int &, int const *const>;
        static_assert(std::is_same_v<typename type::arg_type<0>, char *>);
        static_assert(std::is_same_v<typename type::arg_type<1>, const int &>);
        static_assert(std::is_same_v<typename type::arg_type<2>, int const *const>);
        static_assert(type::arity == 3);
    }

    {
        using objectif = void (S::*)();
        static_assert(std::is_same_v<typename clean_calif<decltype(&S::foo0)>::type, objectif>, "");
        static_assert(std::is_same_v<typename clean_calif<decltype(&S::foo1)>::type, objectif>, "");
        static_assert(std::is_same_v<typename clean_calif<decltype(&S::foo2)>::type, objectif>, "");
        static_assert(std::is_same_v<typename clean_calif<decltype(&S::foo3)>::type, objectif>, "");
        static_assert(std::is_same_v<typename clean_calif<decltype(&S::foo4)>::type, objectif>, "");
        static_assert(std::is_same_v<typename clean_calif<decltype(&S::foo5)>::type, objectif>, "");
        static_assert(std::is_same_v<typename clean_calif<decltype(&S::foo6)>::type, objectif>, "");
        static_assert(std::is_same_v<typename clean_calif<decltype(&S::foo7)>::type, objectif>, "");
        static_assert(std::is_same_v<typename clean_calif<decltype(&S::foo8)>::type, objectif>, "");
        static_assert(std::is_same_v<typename clean_calif<decltype(&S::foo9)>::type, objectif>, "");
        static_assert(std::is_same_v<typename clean_calif<decltype(&S::foo10)>::type, objectif>, "");
        static_assert(std::is_same_v<typename clean_calif<decltype(&S::foo11)>::type, objectif>, "");
        static_assert(std::is_same_v<typename clean_calif<decltype(&S::foo12)>::type, objectif>, "");
        static_assert(std::is_same_v<typename clean_calif<decltype(&S::foo13)>::type, objectif>, "");
        static_assert(std::is_same_v<typename clean_calif<decltype(&S::foo14)>::type, objectif>, "");
        static_assert(std::is_same_v<typename clean_calif<decltype(&S::foo15)>::type, objectif>, "");
        static_assert(std::is_same_v<typename clean_calif<decltype(&S::foo16)>::type, objectif>, "");
        static_assert(std::is_same_v<typename clean_calif<decltype(&S::foo17)>::type, objectif>, "");
        static_assert(std::is_same_v<typename clean_calif<decltype(&S::foo18)>::type, objectif>, "");
        static_assert(std::is_same_v<typename clean_calif<decltype(&S::foo19)>::type, objectif>, "");
        static_assert(std::is_same_v<typename clean_calif<decltype(&S::foo20)>::type, objectif>, "");
        static_assert(std::is_same_v<typename clean_calif<decltype(&S::foo21)>::type, objectif>, "");
        static_assert(std::is_same_v<typename clean_calif<decltype(&S::foo22)>::type, objectif>, "");
        static_assert(std::is_same_v<typename clean_calif<decltype(&S::foo23)>::type, objectif>, "");

        static_assert(
            std::is_same_v<
                typename clean_calif<
                    decltype(&S::test_ptr_cc)>::type,
                void (S::*)(int const *const)>);
    }
    {
        //void test(int, double) const volatile &&  {}
        using type = decltype(&S::test);
        static_assert(std::is_same_v<type_param<type, 0>, int>);
        static_assert(std::is_same_v<type_param<type, 1>, double>);

        auto lbd = [](int, double) {};
        static_assert(std::is_same_v<type_param<decltype(lbd), 0>, int>);
        static_assert(std::is_same_v<type_param<decltype(lbd), 1>, double>);

        const volatile type ptr = &S::test;
        static_assert(std::is_same_v<type_param<decltype(ptr), 0>, int>);
        static_assert(std::is_same_v<type_param<decltype(ptr), 1>, double>);

        const volatile auto ptr2 = &S::test_ptr_cc;
        static_assert(std::is_same_v<type_param<decltype(ptr2), 0>, int const *>);
    }

    std::cout << "clear" << std::endl;
}