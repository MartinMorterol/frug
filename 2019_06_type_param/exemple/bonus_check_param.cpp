#include <iostream>
#include <type_traits>

#include "type_param/function_info.hpp"
#include "type_param/clean_qualif.hpp"
#include "type_param/info_param.hpp"
#include "type_param/can_be_checked.hpp"

template <class T, class U, class... P>
struct check_param
{
};

template <size_t... I, class T, class... P>
struct check_param<T, std::index_sequence<I...>, P...>
{
    static constexpr bool value = (std::is_same_v<
                                       type_param<T, I>,
                                       P> &&
                                   ...);
};

template <bool B, class T, class... Params>
constexpr bool match_param_impl = true;

template <class T, class... Params>
constexpr bool match_param_impl<true, T, Params...> = check_param<
    T,
    std::make_index_sequence<sizeof...(Params)>,
    Params...>::value;

template <class T, class... Params>
constexpr bool match_param = match_param_impl<can_be_checked<T>::value, T, Params...>;
// On ne peux pas faire juste un || ou && car il ne fait pas de compilation conditionnel
void foo(){};
void test_(int, const char *const) {}
int main()
{
    auto lbd = [](int) {};
    auto lbda = [](auto) {};

    static_assert(match_param<decltype(lbd)>);
    static_assert(match_param<decltype(&foo)>);
    static_assert(match_param<decltype(lbda)>);

    auto test_lbd = [](int, const char *const) {};
    auto test_lbda = [](auto, auto) {};
    static_assert(match_param<decltype(test_), int, const char *>);
    static_assert(!match_param<decltype(test_), int, char *>);
    static_assert(match_param<decltype(test_lbd), int, const char *>);

    static_assert(match_param<decltype(test_lbda), int, const char *>);
}