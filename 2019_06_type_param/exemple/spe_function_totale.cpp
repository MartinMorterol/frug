#include <iostream>

using namespace std;

template <class T>
void foo(T) { cout << __PRETTY_FUNCTION__ << endl; }
template <>
void foo(int) { cout << "spe : " << __PRETTY_FUNCTION__ << endl; }
void foo(double) { cout << __PRETTY_FUNCTION__ << endl; }

int main()
{
    foo(string{});
    foo(int{});
    foo(double{});
    foo(float{});
    return 0;
}