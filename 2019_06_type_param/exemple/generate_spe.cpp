#include <list>
#include <deque>
#include <vector>
#include <iostream>
#include <type_traits>
/*template<class Class, class Return, class... Args>
struct bidon<Return (Class::*)(Args...) const & >
{
    typedef Return ( Class::* TypeName )(Args...);
    static TypeName decay_fn(Return (Class::*)(Args...) const & );
};
*/

int main()
{
    using namespace std;
    string name = "clean_calif";
    vector<string> args{"", ",... "};
    vector<string> constness{"", "const "};
    vector<string> volatilness{"", "volatile "};
    vector<string> referenceness{"", "& ", "&& "};
    vector<string> noexceptness{"", "noexcept "};
    for (auto c : constness)
        for (auto v : volatilness)
            for (auto r : referenceness)
                for (auto n : noexceptness)
                {
                    string calif = c + v + r + n;

                    cout << calif << endl;
                }

    cout << "'___' " << endl;
    for (auto a : args)
        for (auto c : constness)
            for (auto v : volatilness)
                for (auto r : referenceness)
                    for (auto n : noexceptness)
                    {
                        string calif = c + v + r + n;
                        string templ = "template<class Class, class Return, class... Args> \n";
                        string spe = "struct " + name + "<Return (Class::*)(Args... " + a + ") " + calif + " >";
                        string body = R"(
{
    using type = Return ( Class::* )(Args...);
};
)";
                        cout << templ << spe << body << endl;
                    }

    cout << "___________" << endl;
    std::vector<std::string> function;
    for (auto c : constness)
        for (auto v : volatilness)
            for (auto r : referenceness)
                for (auto n : noexceptness)
                {
                    string calif = c + v + r + n;
                    function.push_back("foo" + std::to_string(function.size()));
                    cout << "void " << function.back() << "() " << calif << "{}" << endl;
                }

    cout << "___________" << endl;
    const string struct_name = "S";
    cout << " using objectif = void (" << struct_name << "::*) ();" << endl;
    for (const auto &f : function)
    {
        cout << "static_assert( std::is_same_v<typename clean_calif<decltype(&" << struct_name << "::" << f << ")>::type,objectif>,\"\");" << endl;
    }

    return 0;
}