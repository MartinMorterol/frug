#include <iostream>
#include <vector>
#include <type_traits>

#include "type_param/can_be_checked.hpp"

void foo(){};
int main()
{
    auto lbd = [](int) {};
    auto lbda = [](auto) {};

    static_assert(can_be_checked<decltype(lbd)>::value);
    static_assert(can_be_checked<decltype(&foo)>::value);
    static_assert(!can_be_checked<decltype(lbda)>::value);
}