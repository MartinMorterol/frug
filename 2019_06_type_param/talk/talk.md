<!-- marp:true -->
<!-- $theme: gaia -->
<!-- page_number: true -->
# Type traits : 
## application aux paramètres d'un foncteur

---
## Avec un exemple.
<br />
<span style="font-size:130%">

```c++
template<class T>
void foo(T functor)
{
    fonctor("pls compile")
}
```

</span>

 

On voudrait connaitre, à la compilation, le type des paramètres de `functor`

---

# Plan 
- Qu'est-ce-qu'un ==type trait== ?
- Motivation
- Spécialisation
- SFINAE 
- Outils utiles
- Écrire notre type trait

---
## Qu'est-ce-qu'un ==type trait== ?
<br/>

- Une classe qui va nous donner des informations ==à la compilation== sur un type, ou nous donner une transformation de ce type.
- C++ en a un include complet : `<type_traits>`.

---
## A quoi ça ressemble ? 
<br />
<span style="font-size:130%">

```c++
std::is_const<T>::value // => bool
std::is_const_v<T>      // C++17 helper

std::add_const<T>::type // guess :D
std::add_const_t<T>     // C++14 helper
```

</span>



Le principe est simple, voyons ce qu'on peut en faire.

---
## Motivation
`S` étant une classe avec un move et un copy constructeur.

<span style="font-size:100%">

```c++
std::vector<S> v1;
v1.emplace_back();
v1.emplace_back();
```

</span>

####  Est-ce-que le move constructeur est appelé ?
1) Oui.
2) Non.
3) Ça dépend, forcement ça dépasse.

---
## Ça dépend.

Pour assurer que lors du "changement de place" aucune exception ne soit lancée, le `vector` vérifie **à la compilation** si le ==move constructor== est `noexcept` ou pas. 

Si oui, le move constructeur est appelé, sinon ça sera une copie.

#### => [Exemple](../exemple/motivation_vector.cpp) <=

---
## Motivation : 
`static_assert`, le ==Concept== du pauvre

<span style="font-size:XCODE%">

```c++
template<class T>
void stuff() { ... }
```

</span>

#### VS

<span style="font-size:XCODE%">

```c++
template<class T>
void stuff()
{
    static_assert(is_forward_iterator_v<T>,"Some infos");
}
```

</span>

---
## Motivation : les types des paramètres
<br />

- Pour l'exemple.
- Les cas d'application sont limités car cette approche n'est possible que sur les fonctions non surchargées.

--- 

# Les techniques en jeu dans l'écriture des ==types traits==

---

## Spécialisation

La spécialisation (partielle ou non) est le fait de, pour une structure, spécifier une partie des types (ou tous).

Le compilateur choisira la structure la plus spécialisée.
	
<span style="font-size:115%">
  
```c++
template<class T> struct is_const          {...};
template<class T> struct is_const<const T> {...};
```
</span>

---
## Spécialisation partielle et totale

Pour les fonctions :
- Seule la spécialisation totale est autorisée.
- Si une surcharge non template existe elle est prioritaire.
	
#### => [Exemple](../exemple/spe_function_totale.cpp) <=

- Si on a besoin d'une spécialisation partielle sur une fonction:

#### => [On délègue à une structure ](../exemple/spe_function_partielle.cpp) <=

---
## Remarque 1 : on peut spécialiser une variable

<span style="font-size:XCODE%">

```c++
template<class T>
constexpr T var = 1; // il faut que l'on puisse affecter
		     // 1 à la variable

template<>
constexpr int var<int> = 2;

template<class T>
constexpr double var<std::vector<T>> = 3;

```

</span>

---
## Remarque 2 : les paramètres par défaut

Les paramètres manquants sont remplacés par la valeur par défaut avant la résolution de la spécialisation.

<span style="font-size:90%">

```c++
template<class T, class U = void>
constexpr T var = 1;

template<>
constexpr int var<int,void> = 2;

int main(){
    cout << var<double> << var<int> << var<int,int>  << endl;
    //             1          2              1
}
```

</span>

---

## SFINAE 

==Substitution failure is not an error== : Si on arrive pas à résoudre la déclaration : on l'ignore.
- Pour les structures : Dans les types template et les types de spécialisation.
- Pour les fonctions : Dans les types template, les arguments, et le type de retour.
<br/>
 #### => [Exemple](../exemple/sfinae_exemple.cpp) <=

---
## SFINAE : activer sous condition

<br />

#### `std::enable_if`


<span style="font-size:110%">

```c++
template<bool B, class T = void>
struct enable_if {};
 
template<class T>
struct enable_if<true, T> { typedef T type; };

template< bool B, class T = void >
using enable_if_t = typename enable_if<B,T>::type;
```

</span>

---


## SFINAE : Attention aux types par défaut

Ce code ne marche pas, pourquoi ? 

<span style="font-size:90%">

```c++
template< class T,
          class U = typename std::enable_if< 
                is_container<T>::value
            >::type
          >
void foo (const T& t) {...}

template< class T, 
          class U =  typename std::enable_if < 
          	!is_container<T>::value
              >::type
            >
void foo (const T& t) {...}
```

</span>


---


# Les types par défaut ne font pas partie de la signature



<span style="font-size:XCODE%">

```c++
template< class T, class U>
void foo (const T& t);

template< class T, class U>
void foo (const T& t);
```

</span>

Le compilateur trouve une redéfinition de template et arrête la compilation avant que SFINAE entre en jeu.

---


## Une solution ?

<br />

- Ne pas utiliser les paramètres templates, le type de retour est un bon candidat.
- Jouer sur le nombre de paramètres.
- Utiliser des ==template non-type parameter==.

--- 
### Jouer sur le nombre de paramètres.

<span style="font-size:100%">

```c++
template< class T,
          class U = typename std::enable_if< 
                is_container<T>::value
            >::type,
            class = void  // <---- HERE -----
          >
void foo (const T& t) {...}

template< class T, 
          class U =  typename std::enable_if <
          	!is_container<T>::value
            >::type
           >
void foo (const T& t) {...}
```

</span>

--- 
### Utiliser des ==template non-type parameter==.

<span style="font-size:XCODE%">

```c++
template< class T,
          typename std::enable_if< 
                is_container<T>::value
            >::type* ptr = nullptr
          >
void foo (const T& t) {...}

template< class T, 
          typename std::enable_if< 
          	!is_container<T>::value 
            >::type* ptr = nullptr
          >
void foo (const T& t) {...}
```

</span>



---

## C++17 et `if constexpr`

Avant C++17 : 

<span style="font-size:90%">

```c++
template <size_t value>
auto matching_type()
   ->  std::enable_if_t<(value <= 8), uint8_t> ;

template <size_t value>
auto matching_type()
  -> std::enable_if_t<(value > 8 && value <= 16), uint16_t>;

template <size_t value>
auto matching_type()
  -> std::enable_if_t<(value > 16 && value <= 32), uint32_t>;

template <size_t value>
auto matching_type()
  -> std::enable_if_t<(value > 32 && value <= 64), uint64_t>;
```

</span>


----


## C++17 et `if constexpr`

Apres C++17 : 

<span style="font-size:100%">

```c++
template <size_t value>
auto constexpr select_underling_t()
{
    if constexpr (value <= 8) 
    	return std::uint8_t{};
    if constexpr (value > 8 && value <= 16) 
    	return std::uint16_t{};
    if constexpr (value > 16 && value <= 32)
    	return std::uint32_t{};
    if constexpr (value > 32 && value <= 64) 
    	return std::uint64_t{};
}
```

</span>

---
## C++17 et `if constexpr`

On peut quasiment toujours remplacer l'usage de ==type traits + SFINAE== par ==type traits + if constexpr==

<span style="font-size:110%">

```c++
template <class T>
auto foo ()
{
    if constexpr ( type_traits_X) 
        return foo_X();
    if constexpr ( type_traits_Y) 
        return foo_Y();
    return foo_default();
}
```

</span>


---


## Outils utiles et cas classiques
#### Vérifier qu'un type existe sur le type de retour 
Si la fonction doit retourner un double, on ne peut plus faire : 

<span style="font-size:80%">

```c++
auto foo(const T &) -> typename T::value_type
```

</span>

On peut utiliser, le combo `decltype` et `,`

<span style="font-size:80%">

```c++
auto foo(const T &) -> decltype(typename T::value_type{}, double{})
```

</span>

---

#### Que faire si `T::value_type` n'est pas default constructible ?

1) `std::declval` 

<span style="font-size:90%">

```c++
auto foo(const T &)
-> decltype(std::declval<typename T::value_type>(), double{})
```

</span>

2) Ne pas le construire \o/


<span style="font-size:100%">

```c++
template<class Check, class Selected> 
using last_if_valid = Selected ;

template <class T>
auto fonction_sfinae(const T &) 
-> last_if_valid< typename T::value_type, double>
```

</span>

---
#### Que faire si `T::value_type` n'est pas default constructible ?

Et on peut tester plusieurs choses d'un coup avec 

<span style="font-size:XCODE%">

```c++
 last_if_valid< 
    std::void_t< 
        typename T::value_type,
        // ...
        decltype( std::declval<T>().reserve(1) )
    >,
    double
 >
```

</span>

---

## Écrire notre type trait
#### 1. Choisir notre API

<span style="font-size:95%">

```c++

template <class T, auto Nb>
using type_param = /* Do magic here*/ ;

template <class T>
void foo()
{
    static_assert(std::is_same_v<type_param<T,0>,int>);

    auto functor = functorFactory();
    static_assert(
    	std::is_same_v<type_param<decltype(functor),0>,int>
    );
}
```

</span>

---

#### 2. Si on avait les types sous cette forme :

<span style="font-size:XCODE%">

```c++
template<class ReturnType, class ... Args>
struct function_info{
    constexpr static size_t arity = sizeof...(Args);
  
    template <size_t indice>
    struct arg_type_    {
        static_assert ((indice < arity ), "..." );
        using type= typename std::tuple_element<
        	indice, std::tuple<Args...>
            >::type;
    };
    
    using return_type = ReturnType;

    template <size_t i> 
    using arg_type = typename arg_type_<i>::type;
};
```

</span>

---

#### 3. Le cas simple : le pointeur de fonction
<br />

```c++
template <class... T> class ERREUR;
```

```c++
const volatile decltype( &test) p;
ERREUR<std::decay_t<decltype(p)>,decltype(p)> e;
```

```c++
/*p*/     void (* const volatile)(int, double)
/*decay*/ void (*)(int, double) 
```

<br>
La spécialisation fera l'affaire.

---

#### 3. Le cas simple : le pointeur de fonction

<span style="font-size:100%">

```c++
template <class T>
struct informationParam_impl{};

// pointeur de fonction
template < class ReturnType, class... Args>
struct informationParam_impl<ReturnType(*)(Args...)>
{
    using type = function_info<ReturnType,Args...>;
};

// les fonctions avec des args variadic C style
template < class ReturnType, class... Args>
struct informationParam_impl<ReturnType(*)(Args...,...)>
{
    using type = function_info<ReturnType,Args...>;
};
    
```

</span>


---

#### 4. le pointeur de fonction membre
<br />

```c++
const volatile decltype( &S::test) p;
ERREUR<std::decay_t<decltype(p)>,decltype(p)> e;
```

```c++
/*p*/     void (S::* const volatile)(int, double)
/*decay*/ void (S::*)(int, double) 
```


<br>
On peut utiliser la même approche, mais il y a un problème.

---


#### 4. Quid des qualifieurs ?

<span style="font-size:XCODE%">

```c++
struct S {
    void foo() const volatile && noexcept {}
};
const volatile auto p = &S::foo;
```

</span>

`std::decay` ne s'applique que sur le type du pointeur.

<span style="font-size:85%">

```c++
/*p*/     void (S::* const volatile)() const volatile && noexcept
/*decay*/ void (S::*)() const volatile && noexcept
```

</span>

Et le type trait :

<span style="font-size:85%">

```c++
template<class T> struct remove_noexcept
template<class T> struct remove_noexcept<T noexcept > // compile pas
```

</span>


---


#### 4. Du coup on doit gérer les cas suivants (deux fois pour les `...`) :

<span style="font-size:70%">
  
```

noexcept 
& 
& noexcept 
&& 
&& noexcept 
volatile  
volatile noexcept 
volatile & 
volatile & noexcept 
volatile && 
volatile && noexcept 
const  
const noexcept 
const & 
const & noexcept 
const && 
const && noexcept 
const volatile  
const volatile noexcept 
const volatile & 
const volatile & noexcept 
const volatile && 
const volatile && noexcept 
```

</span>

---


#### 4. Quid des qualifieurs ?
<br />

On a deux solutions : 
- trouver une astuce.
- écrire 48 spécialisations

---


#### 4. Quid des qualifieurs ?

<br />


#### J'ai écrit les 48 spécialisations (avec un programme)
#### => [clean qualif](../type_param/clean_qualif.hpp) <=
 
On peut maintenant faire :
 
<span style="font-size:100%">

```c++
 informationParam_impl<
            clean_calif_t<
                std::decay_t<
                	T
                >
            >
       >
```

</span>

---


#### 5. Unifier les fonctions et les ==operator()== 


 
<span style="font-size:100%">

```c++
template < class T,class U = void >
struct type_to_ptr_operator_parenthesis{
    using type = T;
};

template < class T >
struct type_to_ptr_operator_parenthesis<
      T,
      std::void_t<
         decltype(&T::operator())
      >
  >
{
    using type = decltype(&T::operator());
};
```

</span>

--- 
#### 6. On y est ! (presque)
<span style="font-size:90%">

```c++
template < class T,size_t Nth>
using type_param = typename informationParam_impl< //4
         clean_calif_t<                            //3
             type_to_ptr_operator_parenthesis_t<   //2
                 std::decay_t<T>                   //1
             >
         >
     >::type::template arg_type<Nth>;               //5
```
1. On nettoie le pointeur.
2. Si c'est une classe, on prend la référence vers ==operator()==
3. On nettoie si besoin les "qualifieurs d'objet"
4. On passe le tout à notre structure initiale
5. On demande le type du Xem argument

</span>

--- 
#### 7. Vérifier qu'on puisse regarder 

<span style="font-size:90%">

```c++
template <class U, typename T = void>
struct can_be_checked : public std::false_type {};

template <typename U>
struct can_be_checked<
  U,
  std::enable_if_t<
    std::is_function_v<
    	std::remove_pointer_t<U>
>>>  :  public std::true_type{};

template <typename U>
struct can_be_checked<
  U,
  std::void_t<
    decltype(&U::operator())>
> :  public std::true_type{};
```

</span>

---
#### 8. Bonus : regarder tous les paramètres

<span style="font-size:80%">

```c++
template <class T, class U, class... P>
struct check_param{};

template <size_t... I, class T, class... P>
struct check_param<T, std::index_sequence<I...>, P...>{
    static constexpr bool value = (std::is_same_v<
                                       type_param<T, I>,
                                       P> &&
                                   ...);
};

template <bool B, class T, class... Params>
constexpr bool match_param_impl = true;

template <class T, class... Params>
constexpr bool match_param_impl<true, T, Params...> =
check_param<T,
            std::make_index_sequence<sizeof...(Params)>,
            Params...>::value;

template <class T, class... Params>
constexpr bool match_param = match_param_impl<
                        can_be_checked<T>::value, T, Params...>;
```

</span>

---

# Merci de votre attention.
### Question(s)?
